---
title: "About Me"
description: "My name is **Hari Rana** (pronounced as `/ˈhɛəɹi/` (Harry)). I'm known as TheEvilSkeleton, TheEvilSkely, Skelly, and Tesk online."
layout: about
---

<img src="/assets/logo-512x512.png" class="icon" height="256" alt="Logo"/>
<h2 class="no-text-decoration"><code>[Tesk ~]$ whoami <span class="cursor">█</span></code></h2>

{{ page.description }} I'm 21 years old and my pronouns are he/him. I live in Montréal, Canada.

I want to contribute to [free and open source software](https://en.wikipedia.org/wiki/Free_and_open-source_software) and the adoption of the Linux desktop, by advocating various free and open source technologies. I write articles as a hobby. My content are meant for educational purposes.

**I'm looking to [work](/work.html)!**

## Skills
- Shell scripting (bash)
- Software development (Python and C)
- Web development (HTML, CSS, Flask and JavaScript)
- GTK (PyGObject)
- Git and hosting sites (GitHub, GitLab and Gitea)
- Collaborating with other people
- Technical writing
- Research and writing articles
- Flatpak packaging
- Docker/Podman
- Quality Assurance

## Certifications
- [CS50x](https://courses.edx.org/certificates/3c726c5051fb4b9b84a0402770bff4d1) (Harvard University / edX)
- [CS50P](https://courses.edx.org/certificates/911f1d1fb34849af84b997cf58ec2856) (Harvard University / edX)

## Online Presence
- See [projects](/projects)
- [GNOME Foundation member](https://foundation.gnome.org/membership/#:~:text=Hans%20de%20Goede-,Hari%20Rana,-Heather%20Ellsworth)
- [Fedora Editorial board](https://docs.fedoraproject.org/en-US/fedora-magazine/editorial-meetings/#:~:text=chair%20glb%20rlengland-,theevilskeleton,-cverna%20asamalik) and writer at Fedora Magazine
- Fedora Quality Assurance
- This website

## Preferences
I enjoy watching anime and cartoons, but I am extremely picky about them. My favorite anime are [Baccano!](https://en.wikipedia.org/wiki/Baccano!) and [Durarara!!](https://en.wikipedia.org/wiki/Durarara!!), as these stories focus on various characters and different sides of the story. Both stories are written by [Ryōgo Narita](https://en.wikipedia.org/wiki/Ry%C5%8Dgo_Narita).

As for my favorite cartoons, they are [The Last Airbender](https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender) and [The Legend of Korra](https://en.wikipedia.org/wiki/The_Legend_of_Korra). These are my favorites for the same reason as Baccano! and Durarara!!, additionally the character development throughout the story.

My favorite movies are [Kung Fu Panda](https://en.wikipedia.org/wiki/Kung_Fu_Panda) and [How to Train Your Dragon](https://en.wikipedia.org/wiki/How_to_Train_Your_Dragon). Just like The Last Airbender and The Legend of Korra, there's a lot of character development in these stories.

My favorite video game is [Mindustry](https://mindustrygame.github.io/). It is a sandbox tower-defense game that is challenging and requires a lot of dedication.

My favorite song is [Shadow and Truth](https://music.youtube.com/watch?v=B_BRs_DTvqo) from [ONE Ⅲ NOTES](https://music.youtube.com/channel/UCiLnpMrMVih3Y_jd3tvoMsg), and my favorite artist is [auvic](https://soundcloud.com/auvicmusic).

## Hardware
My laptop is a [Dell Inspiron 15 5005 (Option two)](https://web.archive.org/web/20210325222115/https://dl.dell.com/topicspdf/inspiron-15-5505-laptop_users-guide_en-us.pdf). My desktop is a custom built PC from 2017 with the following specs:
- Motherboard: [MSI B350M GAMING PRO](https://www.msi.com/Motherboard/b350m-gaming-pro.html)
- RAM: [2x4GB Team T-Force Vulcan 3200MHz](https://www.teamgroupinc.com/en/product/vulcan-ddr4)
- CPU: [Ryzen 5 1600](https://www.amd.com/en/products/cpu/amd-ryzen-5-1600)
- GPU: [Sapphire Nitro+ RX 580 4GB](https://www.sapphiretech.com/en/consumer/nitro-rx-580-4g-g5)
- PSU: [EVGA 430W](https://www.evga.com/products/specs/psu.aspx?pn=3615c216-ffc7-409a-8536-7cf08f2674a2)
- Case: [MasterBox MB600L](https://www.coolermaster.com/us/en-us/catalog/cases/mid-tower/masterbox-mb600l/)
- Storage: [WD Blue SA510 SATA 250 GB SSD](https://www.westerndigital.com/en-ca/products/internal-drives/wd-blue-sa510-sata-2-5-ssd#WDS250G3B0A)

## Software
### Operating System
At the moment, I use Fedora Silverblue on both my PC and laptop.

### Applications
On Silverblue, I use core GNOME applications to fit the GNOME ecosystem.

- Browser: [Firefox](https://www.mozilla.org/en-US/firefox/new/)
- Shell: [fish](https://fishshell.com/)
- Messaging: [Matrix](https://matrix.org/) ([Element](https://element.io/))
- Password manager: [Bitwarden](https://bitwarden.com/)
- Office: [LibreOffice](https://www.libreoffice.org/)
- Image editor: [GIMP](https://www.gimp.org/)

## Contact
You can contact me on the following platforms:
- Email: [theevilskeleton@riseup.net](mailto:TheEvilSkeleton <theevilskeleton@riseup.net>)<br>(No, I won't share invite links)
- [\[matrix\]](https://matrix.org/): [@theevilskeleton:fedora.im](https://matrix.to/#/@theevilskeleton:fedora.im)
