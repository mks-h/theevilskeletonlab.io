---
layout: post
title: "On the GNOME Project and \"My Way or the Highway\""
description: "I often see people claim that the GNOME Project is anti-collaborative because they follow the \"my way or the highway\" mindset. I would like to explain where this mindset comes from, why this is an aggravated claim, and what you can do if you are negatively affected by the GNOME Project's decisions."
tags: ["GNOME", "Linux"]
toc: true
---

## Disclaimers and Notes
1. To provide examples, I will name certain entities. This is not to shame them in any way.
1. I will be using the following terminology from Wikipedia:
  - [GNOME Project](https://en.wikipedia.org/wiki/GNOME_Project): *"GNOME Project is a community behind the GNOME desktop environment and the software platform upon which it is based"*.<br>"They" pronoun will be used.
  - [GNOME](https://en.wikipedia.org/wiki/GNOME): *"GNOME \[...\] is a free and open-source desktop environment"*.<br>"It" pronoun will be used.
1. I am not speaking on the behalf of the GNOME Project.

## Introduction
For the longest time, the GNOME Project has created many controversies because of their sheer resistance in public pressure, very opinionated and sometimes unpopular stances on the Linux desktop, and, from time to time, dismissive behavior.

While the GNOME Project is not without their faults, {{ page.description }}

## What is "My Way or the Highway"?
In the context of software development, "[my way or the highway](https://en.wiktionary.org/wiki/my_way_or_the_highway)" is often expressed that anyone, be it a member, contributor or user has to adhere to the philosophy, rules and standards of a project, otherwise their proposal or contribution is likely going to be rejected. In this case, "my way" means to adhere to the philosophy, rules and standards of the project, "highway" means the proposal or contribution is rejected.

## Understanding the Mindset
This mindset stems from wanting to fulfill a vision or goal, while complying with a philosophy. If a person proposes an idea that is incompatible with the philosophy of a project, then that project will simply reject it.

The GNOME Project has a vision that it wants to push and perfect. Their philosophy, simply put, is "stay out of my way". The goal is for the user to be able to do their job without being distracted, and to be encouraged to keep everything organized. To achieve this, they put everything away until the user needs something.

GNOME is designed to avoid overwhelming the user with [information overload](https://en.wikipedia.org/wiki/Information_overload), which is a really important topic for usability for the average computer user. It is also designed to make the user's system as clutter-free as possible. Therefore, it does not follow the traditional desktop paradigm, where there is a taskbar, desktop icons, and others - everything is put away until the user needs them. Likewise, applications also implement [hamburger menus](https://pointieststick.com/2021/04/03/how-i-learned-to-stop-worrying-and-love-the-hamburger-menu) to put everything away.

This means, if one proposes the GNOME Project to implement a taskbar with an application menu, a [menu bar](https://en.wikipedia.org/wiki/Menu_bar), or any addition or change that encourages GNOME to resemble something that it isn't supposed to be, then the GNOME Project will reject it. However, if one proposes something that improves and builds upon the GNOME Project philosophy, then it will be taken into consideration.

This type of behavior is consistent with numerous projects, including Firefox, the Linux kernel, KDE and many, many others. Even much smaller projects, like Sway, have a vision that they'd want to perfect. These projects have a scope in which they'd accept proposals and contributions. They have rules and standards that people must adhere to contribute to that project.

As an example, if I propose the Linux kernel developers to port Linux to a microkernel, then the developers will reject it. Linux's philosophy is "[\[t\]o not break userspace](https://lkml.org/lkml/2012/12/23/75)". Porting to another kernel architecture will literally go against Linux's philosophy: it will break many workflows in different ways, which can easily negatively affect systems at a large scale, like infrastructures, businesses, etc. Thus, the Linux kernel developers prefer to stick with the older monolithic architecture to simply avoid breaking systems and workflows.

As another example, if I propose KDE to adopt the GNOME philosophy and designs for Plasma, then KDE will reject this proposal, as it goes against their philosophy. Plasma is designed to provide a powerful desktop while following the traditional desktop paradigm, whereas GNOME is the opposite.

Even though those examples may seem over exaggerated, they go against the corresponding projects' philosophies. Many people and companies regularly propose the GNOME Project to go against their philosophy, the same way many people and companies ask the Linux kernel developers to break userspace.

## Why Does Everyone Only Talk About GNOME?
*Note: this section is opinionated.*

When it comes to "my way or the highway", I noticed that users mainly reference GNOME, far more than any other project. There is one main reason I can think of.

GNOME is the most used desktop environment, therefore it is the most susceptible to gain demands and pressure from users and companies. Furthermore, it is also the most susceptible to gain media attention, like Reddit, news outlets, and many more. Even worse, this also attracts internet trolls from 4chan and Hacker News (I'm not kidding) and people who spread misinformation and disinformation.

Since a much larger portion of Linux desktop and server users use GNOME more than anything else, there are also more users who propose (and often even demand) the GNOME Project to add or make a change that goes against the philosophy. Consequently, it is a lot more common for the GNOME Project to reject those proposals. As a result, more users have a negative experience with the GNOME Project and come to the conclusion that the "my way or the highway" mindset only applies to the GNOME Project. Meanwhile, those users may likely feel comfortable with another desktop environment, so no proposals need to be made, and thus the developers of that respective project are considered as "good", because the user has no experience with proposing something that goes against the philosophy.

People often propose the GNOME Project to be something that it is not, in which the GNOME Project has to repeatedly refuse. Since GNOME's workflow is opinionated, many people disagree with it. The majority of people who switch to or try out Linux come from Windows, so it's common for them to propose something that looks familiar to Windows, i.e. the traditional desktop paradigm. As mentioned before, misinformation, disinformation, and perhaps false conclusion heavily come into play too, as they often mislead people who are unaware of the GNOME Project's philosophy and convince them based on the negativity towards the GNOME Project.

## Solution
If you dislike the GNOME Project's philosophy, then the solution is to simply not use GNOME. The Linux desktop is not Windows or macOS; it is built around options. If you dislike something, then you can use an alternative by swapping components or migrating to another Linux distribution. *You*, as a tinkerer, are in control over your own operating system. So, the onus is on you to install or develop anything that complies with your standards.

It is why distributions, like Arch Linux, NixOS and Gentoo exist. It is also why other desktop environments, like Plasma, still exist. Plasma is designed to be user-*centric* and balances with user-friendliness, whereas GNOME, on the other hand, is strictly designed to be user-*friendly*. It is completely normal that someone who prioritizes user-centrism would want to avoid GNOME and the GNOME Project in general. Heck, if I want to customize and tinker with my system, then I'd use Plasma or something else any day.

## Conclusion
In the end, using "my way or the highway" as an argument against the GNOME Project is aggravating, as the majority of projects largely follow this mindset. These projects have a set of rules and standards that people must adhere to if they want to be a part of.

A project exists to push and perfect its vision to their target audience. It is up to you, as a user, to decide whether you agree with them or not. That being said, you have all the power to switch to another software. If GNOME isn't for you, then you have Plasma. If Plasma and GNOME aren't for you, then you have other desktop environments and window managers to try. Don't forget, the Linux desktop is not Windows or macOS.

---

- Edit 1: Correct typos
- Edit 2: Add missing information
- Edit 3: Clarify user-friendliness/centrism of DEs
