---
layout: post
title: "A Letter to Microsoft for not Attributing Authors of the Edge Flatpak Application"
description: "Microsoft recently [posted an article](https://support.microsoft.com/en-us/topic/xbox-cloud-gaming-in-microsoft-edge-with-steam-deck-43dd011b-0ce8-4810-8302-965be6d53296) and an [announcement](https://www.reddit.com/r/MicrosoftEdge/comments/th77w9/microsoft_edge_beta_the_browser_for_xbox_cloud/) about cloud gaming on the Steam Deck. The authors promoted the Edge Flatpak application in a way that would lead people to believe that Microsoft itself is the maintainer of the Flatpak application when that isn't the case. I am the maintainer and author of the Edge Flatpak application, [re:fi.64](https://refi64.com/) is the author of the [Chromium base app](https://github.com/flathub/org.chromium.Chromium.BaseApp) and the person who got Chromium working inside a Flatpak container with [Zypak](https://github.com/refi64/zypak). And many other [contributors](https://github.com/flathub/com.microsoft.Edge/graphs/contributors) have been the ones who continued improving the Flatpak application."
tags: ["Linux", "Flatpak", "Microsoft"]
---

---

**Update**: Kyle Pflug, a Product Manager for Edge, reached out to me via email. He indeed confirmed it was an oversight and sincerily apologized. He mentioned that Microsoft is working with the engineering team for official Flatpak releases. I offered to create Flatpak manifests for stable and dev branches.

**Update 2**: Kyle Pflug updated the [support article](https://support.microsoft.com/en-us/topic/xbox-cloud-gaming-in-microsoft-edge-with-steam-deck-43dd011b-0ce8-4810-8302-965be6d53296#:~:text=Note%3A%C2%A0This%20article,at%20this%20time.) and [Reddit post](https://www.reddit.com/r/MicrosoftEdge/comments/th77w9/microsoft_edge_beta_the_browser_for_xbox_cloud/#:~:text=we%E2%80%99d%20like%20to%20thank%20u%2Ftheevilskely%20and%20the%20community%20of%20contributors%20who%20maintain%20the%20unofficial%20microsoft%20edge%20beta%20flatpak%20package%20%E2%80%93%20without%20them%20this%20guide%20wouldn%E2%80%99t%20be%20possible.) and the team has properly attributed us maintainers. They have also mentioned the Flatpak application is unofficial and maintained by the community.

Kyle Pflug explained that Microsoft is in touch with Flathub maintainers, however it is busy with other tasks, so there are no promises about when there will be official support, or if there will ever be.

Microsoft is also planning to ship Edge with tar archives, so this should facilitate packaging, which includes Flatpak.

I'd like to thank Kyle Pflug for the very quick responses! I was reached out by him about 4 hours after the publishing of this article. Bonus points for reaching out during the weekends too. The article and announcement were quickly edited within a couple of days.

---

{{ page.description }}

[I have reached out](https://github.com/MicrosoftEdge/Status/issues/704) to Microsoft in the past asking if they would be interested in officially maintaining the Flatpak application, to which I received no official answer. I would happily work for Microsoft on the open-source team they’ve created as the official maintainer of the Edge Flatpak application since I already have experience maintaining it, and this would also prevent Microsoft from having to train someone else to do what I am already doing. I have happily kept the Edge Flatpak application alive and updated voluntarily for more than a [year](https://github.com/flathub/flathub/pull/1992#issuecomment-748832842), with no support from Microsoft. It doesn’t feel right to have our efforts ignored.

Needless to say, this is most likely an oversight so I have not concluded that this was intentional, yet. Microsoft is a huge company, so the Xbox Cloud Gaming team was probably unaware of Edge Flatpak's status. However, looking back in time, Microsoft has a history of using other people's work without properly crediting the original authors' work. A notable example is the AppGet and WinGet incident.<sup>[[1]](https://keivan.io/the-day-appget-died/)[[2]](https://www.zdnet.com/article/microsoft-credits-maker-of-package-manager-it-copied-for-windows-10-but-offers-no-apology/)</sup>

If this was indeed an oversight, then, in my opinion, it would be reckless of them to not check with internal teams to see who maintained the Edge Flatpak application. If there was a malicious actor maintaining it, pointing to the package without taking ownership or having some form of contract could be quite dangerous for the people who use it, and for Microsoft's reputation.

I would like to kindly ask Microsoft to properly attribute us maintainers and contributors for tirelessly maintaining the Flatpak application and providing the best support and user experience voluntarily.

I would also like to ask Microsoft to officially maintain the Edge Flatpak application, either by co-maintaining, or independently maintaining by itself. This will get more companies and maintainers interested and involved to the adoption of Flatpak and its technologies like [portals](https://opencollective.com/flatpak/projects/portals). Microsoft could also donate to [Flatpak](https://opencollective.com/flatpak) for making it possible to share Edge so easily across Linux.

---

I would like to thank my friends Deathwish, [Lionir](https://thelion.website/), [re:fi.64](https://refi64.com/), and [Tyler](https://cipherops.space) for proofreading and helping to improve this letter.
