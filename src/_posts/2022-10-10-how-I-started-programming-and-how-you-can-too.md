---
layout: post
title: "How I Started Programming, and How You Can Too"
description: "I am writing this article on my birthday to give my thanks and appreciations to those who helped me start and continue my journey with programming. I want to return the favor by explaining how I started programming, for those who are struggling with getting started with programming, and give them some motivation to continue their journey."
tags: ["Programming"]
toc: true
---

## Introduction
{{ page.description }}

I've had a lot of trouble getting started with programming. About 6 years ago, I tried reading free books and documentation online all by myself. The complications and assumptions from these resources caused me to lose motivation very quickly. I tried this approach several times every couple of months, but the results were consistent -- it always ended with me giving up and not making much progress.

Last year, I tried a completely different approach: taking a course from Harvard University, and then contributing to free and open source projects. This approach was really effective and got me to a point where I joined the [Bottles] project, one of the most [popular applications](https://archive.ph/WpVSp) on Flathub. And later became a member of the [GNOME Foundation], a leading organization on the Linux desktop.

## Online Courses From Known Institutions
The very first step I suggest is to take an online course from a well-known institution, by listening to lectures and completing assignments. I chose [CS50's Introduction to Computer Science], by Harvard University. The course itself, learning materials, assignments and grading are entirely free of cost, but you can optionally pay for a certification.

The goal of this course is to provide resources and explain the fundamental basics of programming for those who have no prior experience with programming. It is self-paced. You can get started at any time, and you can transfer your assignments the following year to continue your work, without starting over. There are 11 classes of CS50, from week 0 to week 10. For simplicity, a week can be considered as an "episode". Each week has at least one assignment.

Bear in mind that the course is challenging and time consuming. However, CS50 is active in several platforms, so you can ask the CS50 community for help. In my experience, the community was really friendly, answered my questions very clearly and was fast at responding.

I struggled a lot with CS50 because of their challenging assignments, but I noticed that it was the only time I spent more than a month with programming without giving up at all. In my opinion, this challenge is worth accepting, and I highly recommend starting with CS50, as there's a big community contributing to it.

## Opening up the Possibilities
After completing from week 0 to week 8 of CS50 and from the knowledge and experience you gained from those weeks and assignments, I suggest to contribute to free and open source projects that interest you, especially ones that you regularly use. For me, it took me roughly 4 months to complete from week 0 to week 8, as I struggle with concentration.

You can contact the maintainer of your preferred project and ask if there are issues that need to be addressed, such as bugs or feature proposals. If there is an issue that you'd like to tackle, then you can ask the maintainer to assign you to that issue and provide guidance if need be. I decided to contribute to Bottles, which helped me expand to different projects.

### Bottles
I was interested in [Bottles] for a long time, as I used it a lot (and still do), so it was my first pick. I noticed that the project is sophisticated and beyond my level of comprehension. Instead of postponing my contributions to Bottles, I attacked it head on. I suggest to have a similar mentality as well, unless you firmly believe that the project in question is *too* sophisticated. This was when I met [Mirko Brombin], the founder and maintainer of Bottles.

My first code contributions to Bottles were correcting typos. After some time, I got the idea to create a dialog for [vkBasalt] as a [feature proposal](https://github.com/bottlesdevs/Bottles/issues/1607), as I wanted to learn more about vkBasalt and felt like it would be really useful for Bottles. Since I had very little knowledge with Bottles's codebase, I asked many questions. I could say more than 100 questions. He tutored me with this subproject that later became my very first major contribution to the project.

Two months later of hard work, my massive 1000+ lines [contribution](https://github.com/bottlesdevs/Bottles/pull/1668#event-7142635031) was finally introduced to Bottles and later as an [announcement](https://usebottles.com/blog/release-2022.8.14/)! Since then, I continue to contribute to Bottles, and Mirko became a good friend of mine.

#### "Help Me Help You"
This method I call "help me help you". It's where I ask the maintainer of a project to help me with making my first contributions, so I can help them back with any kinds of contributions, be it code, quality assurance, documentation, etc. This is really effective, as it got me to understand Bottles, Python and GTK. I heavily encourage others to try this method.

### Other Projects
With the knowledge I gained from my experience with Bottles, I started contributing to other GTK projects. I ported [Fractal](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/1131) and [Workbench](https://github.com/sonnyp/Workbench/pull/150) to the new About window ([AdwAboutWindow](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.AboutWindow.html)). At the time of writing this article, I am helping rework the user interface of [Tubefeeder](https://github.com/Tubefeeder/Tubefeeder/issues/95).

## Why Did I Fail Initially?
My biggest struggle with programming wasn't even programming itself, but my mentality, and I feel like this is the case for many people who struggle with programming. For a long time, I had the mindset of doing everything myself, without asking for help. I had a fear of asking stupid questions and people making fun of me, so I prevented both for 6 years.

However, I didn't notice the harm it caused to my mental health and opportunities. Unfortunately, this ignorance costed me 6 years of my life. If you have the fear of asking stupid questions, then my advice to you is: what's more stupid than asking stupid questions is to not ask the question, so ask your damn question. By avoiding to ask potential stupid questions, you are actively preventing yourself from asking good questions, which can literally hinder your learning experience. I urge people to not repeat my mistake.

If your "tutor" makes fun of you or is condescending to you, then I advise you to politely ask them to stop. After all, maintainers are humans too, so this kind of behavior can potentially happen; giving them another chance can be beneficial to long-term relationships between maintainers and potential contributors. If they keep this attitude by not respecting your request, then I argue they lost a valuable potential contributor. In that case, I suggest to contribute to another project. Don't let this incident stop you, but use it as a means to help you deal with similar scenarios in the future.

## Conclusion
I struggled with programming for a really long time, due to my ignorance and fears. Every decision you take in your life comes with at least one compromise.

At the beginning, I prioritized comfort, but unnoticeably compromised opportunities. Last year, I did the other way around, which not only increased my opportunities to learn new things, but in turn got me really comfortable with the outcome.

When you take a decision, try to look at what you are compromising. See if this compromise is worth addressing and address it if it is. Ask for help when you are certain you need help -- and be prepared to be made fun of, so you can continue your journey without it negatively affecting the future.

I want to thank [Mirko Brombin], the staffs at CS50 and the community for guiding me through my journey in programming. I hope this gives the motivation to anyone who is struggling with programming, or prevent potential learners from repeating my mistake.

---
Edit 1: Improve sentences and clarity

[CS50's Introduction to Computer Science]: https://www.edx.org/course/introduction-computer-science-harvardx-cs50x
[Bottles]: https://usebottles.com/
[GNOME Foundation]: https://gitlab.gnome.org/Teams/MembershipCommittee/-/issues/308
[Mirko Brombin]: https://mirko.pm/
[vkBasalt]: https://github.com/DadSchoorse/vkBasalt

