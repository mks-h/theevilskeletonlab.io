---
layout: post
title: "What Not to Recommend to Flatpak Users"
description: "Whenever I browse through the web, I find many \"tips and tricks\" from various blog writers, YouTubers and others who recommend users to take steps that either they aren't supposed to, or have better alternatives. In this article, I will go over some of those steps you should not be taking and explain why."
tags: ["Linux", "Flatpak"]
toc: true
---

## Introduction
{{ page.description }}

## Setting GTK_THEME
The `GTK_THEME` variable is often used to force custom themes for GTK applications. For example, setting `GTK_THEME=adw-gtk3-dark` will set the dark variant of [adw-gtk3] if installed on Flathub.

`GTK_THEME` is a [debug variable]. It is intended to be used for testing stylesheets for GTK3 and GTK4. However, it is NOT intended to be used by users. libhandy and libadwaita ship additional widgets that the majority of GTK3 and GTK4 themes don't support, as they are made for GTK3 and/or GTK4 alone. This means that using a custom theme on a GTK4+libadwaita application may remove libadwaita widgets.

Many applications are increasingly porting from GTK3 to GTK4+libadwaita. While `GTK_THEME` may work fine on GTK3, the application will appear [broken] after it gets ported to GTK4+libadwaita, if `GTK_THEME` is kept. The solution in that case is to unset `GTK_THEME`.

When recommending `GTK_THEME`, ensure that the user knows that they will need to unset that variable after the application gets ported to GTK4+libadwaita. Or better yet, don't recommend debug variables to users. Otherwise, they will get the impression that the application itself is buggy and not working as intended. They won't know notice that `GTK_THEME` caused it.

## Aliasing flatpak run
A common recommendation is to alias `flatpak run`. When launching Flatpak applications from the terminal, it's typical to type `flatpak run $APP_ID`, where `$APP_ID` is the application ID, for example `org.mozilla.firefox` for Firefox. So, logically, users find that `flatpak run` is too long, so they alias it to, e.g. `fr`.

While this works, there is a better way to improve this situation. Flatpak has its own `/bin` directories that we can add to `PATH`. For system installations, the directory is located at `/var/lib/flatpak/exports/bin`. For user installations, it's located at `~/.local/share/flatpak/exports/bin`.

After we restart the shell, we should be able to use application IDs only, without the need to type `flatpak run`. These directories are not set to `PATH` by default to avoid Flatpak's `/bin` from clashing over distribution packages' binaries that follow the [reverse domain name notation] convention.

`flatpak run` is often used to temporarily add or remove permissions when running the Flatpak application. For example, `flatpak run --nofilesystem=home $APP_ID` denies access to `filesystem=home` for `$APP_ID` for that session specifically.

## Placing Themes and Cursors in ~/.icons and ~/.themes
A major mistake users often make is overriding filesystem permissions to allow read access to `~/.icons` and `~/.themes`. These aforementioned paths are entirely unsupported by Flatpak, as they are legacy directories.

A user who uses a cursor from `~/.icons` may encounter an issue where Flatpak applications fall back to the [Xlib cursor]. And a user who uses `~/.themes` may encounter an issue where Flatpak doesn't automatically detect the theme and install it (if available).

Flatpak heavily relies on XDG standards and thus honors XDG compliant equivalent paths:
- `~/.icons` → `~/.local/share/icons`
- `~/.themes` → `~/.local/share/themes`

It is best to use these XDG compliant paths to avoid overriding permissions, as they are better supported long-term. If you use a program that installs cursors/icons or themes in legacy paths, contact the developers and kindly ask them to follow XDG standards instead!

---
Edit 1: Correct mistake (Credit to [re:fi.64])

[debug variable]: https://docs.gtk.org/gtk4/running.html#gtk_theme
[broken]: https://github.com/bottlesdevs/Bottles/issues/1663
[Xlib cursor]: https://www.oreilly.com/library/view/xlib-reference-manual/9780937175262/images/p099-001.jpg
[adw-gtk3]: https://github.com/lassekongo83/adw-gtk3
[reverse domain name notation]: https://en.wikipedia.org/wiki/Reverse_domain_name_notation
[re:fi.64]: https://refi64.com/
