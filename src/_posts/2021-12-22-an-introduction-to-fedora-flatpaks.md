---
layout: post
title: "An introduction to Fedora Flatpaks"
toc: true
tags: ["Linux", "Fedora", "Flatpak", "Containers"]
description: "Flatpak is a distribution agnostic universal package manager leveraging [bubblewrap](https://github.com/containers/bubblewrap) to separate applications from the system, and [OSTree](https://ostreedev.github.io/ostree/) to manage applications. There are multiple Flatpak repositories (remotes in Flatpak terminology), such as [Flathub](https://flathub.org/home) (the de-facto standard), [GNOME Nightly](https://wiki.gnome.org/Apps/Nightly), [KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications) and finally Fedora Flatpaks, Fedora Project’s Flatpak remote."
---

## Introduction

*Article originally posted on [Fedora Magazine](https://fedoramagazine.org/an-introduction-to-fedora-flatpaks/).*

{{ page.description }}

This article explains the motivation behind Fedora Flatpaks, how to add the remote, how to use it and where to find resources.

## What is Fedora Flatpaks?

Fedora Flatpaks is a Flatpak remote by the Fedora Project for Fedora Linux. However, thanks to the universality of Flatpak, most other distributions can utilize it without a problem. Fedora Flatpaks builds from existing Fedora packages to ensure that everything remains free and open source and complies with Fedora’s standards.

Technically speaking, Fedora Flatpaks reuses existing RPMs from the Fedora Linux repositories and converts them to Flatpak applications using several tools.

## Adding the Fedora Flatpaks Remote

On Fedora Linux, Fedora Flatpaks is already added and ready to go.

If you are using a distribution other than Fedora Linux, then you will have to manually add the desired remotes. At the moment, there are two different remotes: the stable remote for stable applications and the testing remote for testing applications. To add the stable remote, run the following command:

```
flatpak remote-add --if-not-exists fedora oci+https://registry.fedoraproject.org
```

To add the testing remote, run the following command:

```
flatpak remote-add --if-not-exists fedora-testing oci+https://registry.fedoraproject.org#testing
```

These commands may need elevated privileges, thus needing an administrator password. If you do not have access to root or an administrator password, then you can still add the remote by using the `--user` flag to add per-user. If you have used `--user`, then you will have to use it in the later examples too.

## Using Fedora Flatpaks

### Software Center

Flatpak is built with the Linux desktop in mind. Application stores such as GNOME Software have the ability to install and remove Flatpak applications after you add a Flatpak remote, making it easy to manage applications.

On GNOME Software, visiting an application’s page and pressing on the Source button at the top right hand side opens the list of available of sources. By default, on Fedora Linux, GNOME Software selects *Fedora Linux (RPM)*. *Fedora Linux (Flatpak)*, provided by Fedora Flatpaks, is available as an available source, but is not used by default. Simply select it, and then press on the “Install” button.

For example, to install Firefox from Fedora Flatpaks, head over to the Firefox page on GNOME Software. Then, press on the Source button at the top right hand side. Once the menu pops up, press *Fedora Linux (Flatpak)*. Lastly, press *Install*. Here is a visual example:

|---------|---------|
| [![](https://fedoramagazine.org/wp-content/uploads/2021/12/gnome-software1-768x523.png)](https://fedoramagazine.org/wp-content/uploads/2021/12/gnome-software1-768x523.png) Firefox on GNOME Software, with *Fedora Linux (RPM)* as the default option and *Fedora Linux (Flatpak)* as the second option | [![](https://fedoramagazine.org/wp-content/uploads/2021/12/gnome-software2.png)](https://fedoramagazine.org/wp-content/uploads/2021/12/gnome-software2.png) *Fedora Linux (Flatpak)* source ticked |

Afterwards, GNOME Software will install Firefox on your system. You can use the application launcher to launch Firefox, just like any application.

To remove the application, simply press on the trash button next to the blue *Open* button in GNOME Software.

## Command-line Interface

Flatpak uses standard package management terminologies when it comes to commands. Some examples include:

```
# Installing a package
flatpak install fedora $APPLICATION
# Removing a package
flatpak remove $APPLICATION
# Updating packages
flatpak update
```

Substitute `$APPLICATIONS` with the desired application. Firefox for example is *org.mozilla.firefox*, or *firefox* for short. For more information on the commands, refer to the [Using Flatpak documentation](https://docs.flatpak.org/en/latest/using-flatpak.html).

## Finding Resources

### Source Code

For curious people, source codes of container and Flatpak manifests are available on the *flatpaks* namespace at [src.fedoraproject.org/flatpaks](https://src.fedoraproject.org/projects/flatpaks/%2A).

### Filing a Bug

Experiencing a bug with an application? Consider filing an bug! The Fedora Project treats applications from Fedora Flatpaks the same as their RPM counterparts, therefore the process of filing bugs for specific apps is the same as filing a bug for any package on Fedora Linux. To file an bug, head over to [docs.fedoraproject.org](https://docs.fedoraproject.org/en-US/quick-docs/howto-file-a-bug/) and carefully read the instructions.

## Conclusion

In conclusion, Fedora Flatpaks is a remote by the Fedora Project wherein Fedora Linux packages are converted to Flatpak. The vast majority of applications are free and open source. They are tested and verified by the Fedora Project. On Fedora Linux, the Fedora Project includes Fedora Flatpaks for you. On other distributions, you can easily add the remote by simply running a command.
