---
title: TheEvilSkeleton
description: "Welcome to my personal website! I talk about computers, GNU/Linux ecosystems, free and open source, and other topics related to software."
layout: home
---

<img src="/assets/logo-512x512.png" class="icon" height="256" alt="Logo"/>
<h2 class="no-text-decoration"><code>[Tesk ~]$ cat /etc/motd <span class="cursor">█</span></code></h2>

{{ page.description }} You can read my articles and information about myself in the header.

<h2>Donate</h2>
Feel free to support my work by donating to me on one of the following platforms:
- [GitHub](https://github.com/sponsors/TheEvilSkeleton)
- [LiberaPay](https://liberapay.com/TheEvilSkeleton)

<h2>Socials and Contributions</h2>
You can ~~stalk~~ find me on the following platforms:
- Codeberg: [TheEvilSkeleton](https://codeberg.org/TheEvilSkeleton/)
- GitHub: [TheEvilSkeleton](https://github.com/TheEvilSkeleton)
- GitLab: [TheEvilSkeleton](https://gitlab.com/TheEvilSkeleton)
- GNOME GitLab: [TheEvilSkeleton](https://gitlab.gnome.org/TheEvilSkeleton)
- Mastodon: <a rel="me" href="https://fosstodon.org/@TheEvilSkeleton">@TheEvilSkeleton@fosstodon.org</a>
- Twitter: [@JaakunaGaikotsu](https://twitter.com/JaakunaGaikotsu)
- Reddit: [u/TheEvilSkely](https://www.reddit.com/user/TheEvilSkely)
